#include "tacho.h"

#include "WProgram.h"

#include <avr/interrupt.h>
#include <avr/io.h>

//-----------------------------
// some Global constants
class Constant
  {
  public:
    enum
      {
      AnalogPin = 3,

      // 1ms/125uS = 8 samples per ms
      SamplesPerMs = 1000 / 125,

      // How long to accumulate samples in ms
      // must be less than 1000
      TotalSampleTimeMs = 100,

      // 8000 samples is 1 second of samples
      MaxSamples = TotalSampleTimeMs * SamplesPerMs,

      //TODO set lower threshold to lower value for hysteresis
      LowerThreshold = 700,
      UpperThreshold = 700,
      };
  };

//-----------------------------
// Global variables for this cpp
static struct
  {
    // variable to store the converted value
    unsigned int v;

    // whether the current analog state is high or low
    int state;

    // the number of high to low transitions
    int count;
    int samples;
    bool busy;

    void init()
      {
      v = 0;
      state = 1;
      count = 0;
      samples = 0;
      busy = false;
      }
  } Global;

// ===========================
ISR(TIMER1_COMPA_vect)
  {
  if (!Global.busy)
    {
    //TODO: stop the timer
    return;
    }

  Global.samples++;
  if (Global.samples > Constant::MaxSamples)
    {
    // signal main loop we're done
    Global.busy = false;
    }
  else
    {
    Global.v = analogRead(Constant::AnalogPin);
    if (Global.state == 1)
      {
      if (Global.v <= Constant::LowerThreshold)
        {
        Global.count++;
        Global.state = 0;
        }
      }
    else // state == 0
      {
      if (Global.v > Constant::UpperThreshold)
        {
        Global.state = 1;
        }
      }
    }
  }

//===========================
Tacho::Tacho()
    : mLastValue(0)
  {
  }

//===========================
int Tacho::lastValue()
  {
  return mLastValue;
  }

//===========================
// Generating a square wave with a period twice the timeout
//    125uS * 2 = 250uS period
// should see this frequency on oscope:
//    1 / 250 uS = 4000Hz

// calculate number of ticks
// the master clock is 16Mhz
//    1 / 16Mhz = 62.5ns per tick
// the timeout requires this many ticks
//    125uS / 62.5nS = 2000 ticks
// use prescalar of 8
//    2000 / 8 = 250 prescalar ticks
void Tacho::setupEvery125uS()
  {
  // set prescalar to "/8" = 010
  TCCR1B = _BV(CS11);

  // set WGM to CTC mode (010)
  // In this mode Timer1 counts up until it matches OCR1A
  TCCR1A = _BV(WGM11);

  // These are actual measurements from oscope:
  OCR1A = 249;

  // When the OCR1A register matches the Timer1 count, cause an interrupt
  TIMSK1 = _BV(OCIE1A);
  }

//===========================
void Tacho::setup()
  {
  // set up timer to interrupt every 125uS
  setupEvery125uS();
  }

//===========================
int Tacho::measure()
  {
  Global.count = 0;
  Global.samples = 0;

  //TODO turn on timer (if it was off)

  // kick off the sampling session
  Global.busy = true;

  // wait for sampling to finish
  int d = Constant::TotalSampleTimeMs / 8;
  if (d < 10)
    d = 10;
  while (Global.busy)
    {
    delay(d);
    }

  // stopped counting, so it is safe to calculate
  // The count / 4 to get revs per total sample time (there are 4 faces on the rectangular tubing)
  //  * 1000 / TotalSampleTimeMs to get revolutions per second
  //  * 60 to RPM
  mLastValue = ((long) Global.count * 60L * 1000L) / (4L * (long) Constant::TotalSampleTimeMs);
  return (mLastValue);
  }
