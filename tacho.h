#ifndef TACHO_H_
#define TACHO_H_

//===========================
class Tacho
  {
  public:
    //-----------------------------
    Tacho();

    //-----------------------------
    // initialize the tachometer
    void setup();

    //-----------------------------
    // start a measurement
    // return the current RPM
    int measure();

    //-----------------------------
    // return the last RPM reading
    int lastValue();

  private:
    void setupEvery125uS();

  private:
    int mLastValue;
  };

#endif /* TACHO_H_ */
