#ifndef MOTOR_H_
#define MOTOR_H_

#include <stdint.h>

//===========================
class Motor
  {
  public:
    //===========================
    // some common parameters for run() function
    enum
      {
      stopped = 0,
      forward = 1,
      backward = 2,

      faster = 0,
      slower = 1,
      };

    //-----------------------------
    Motor(int motorid);

    //-----------------------------
    // initialize the motor to zero speed
    void setup();

    //-----------------------------
    // idle the motor for a while
    void idleFor(int mstimeout);

    //-----------------------------
    // idle the motor
    void idle();

    //-----------------------------
    // run the motor, either forward or backward,
    // at the given rate (0 = stopped, 255 = full)
    void runAt(uint8_t dir, uint8_t rate);

    //-----------------------------
    // run the motor, either forward or backward,
    // once it's running either increase/decrease the speed
    void run(uint8_t dir, uint8_t rate);

  private:
    uint8_t mId;
    uint8_t mDir;
    uint8_t mSpeed;

    // hide classes used only for implementation
    class Private;
    Private& hidden;
  };

#endif /* MOTOR_H_ */
