#include "motor.h"
#include "tacho.h"

#include "WProgram.h"

//===========================
Motor motor1(1);
Tacho tacho;

//===========================
void setup()
  {
  Serial.begin(9600);
  Serial.println("Motor Control!");
  tacho.setup();
  motor1.setup();
  }

// the expected rpm.
int exp_rpm = 8000;

// the starting rate
int rate = 100;

//===========================
// run the motors forward and back
void loop()
  {
  motor1.runAt(Motor::forward, rate);
  int rpm = tacho.measure();
  Serial.print("rpm: ");
  Serial.print(rpm, DEC);
  Serial.print(" rate: ");
  Serial.print(rate, DEC);
  Serial.println();

  // bump the rate up or down to hit the expected rpm
  if (rpm < (exp_rpm - 1000))
    rate += 10;
  else if (rpm < (exp_rpm - 250))
    rate += 3;
  else if (rpm < (exp_rpm - 150))
    rate += 2;
  else if (rpm < (exp_rpm - 50))
    rate += 1;
  else if (rpm >= (exp_rpm - 50) && rpm <= (exp_rpm + 50))
    ; // skip
  else if (rpm > (exp_rpm + 1000))
    rate -= 10;
  else if (rpm > (exp_rpm + 250))
    rate -= 3;
  else if (rpm > (exp_rpm + 150))
    rate -= 2;
  else if (rpm > (exp_rpm + 50))
    rate -= 1;

  // keep it sane
  if (rate < 0)
    rate = 0;
  else if (rate > 255)
    rate = 255;
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }
