#include "motor.h"

#include "AFMotor.h"
#include "WProgram.h"

//-----------------------------
class Motor::Private
  {
  public:
    Private(int id) :
        motor(id)
      {
      }

    AF_DCMotor motor;
  };

//-----------------------------
static int convertDir(int dir)
  {
  if (dir == Motor::forward)
    return FORWARD;
  if (dir == Motor::backward)
    return BACKWARD;

  return RELEASE;
  }

//-----------------------------
Motor::Motor(int motorid) :
    mId(motorid), mDir(stopped), mSpeed(0), hidden(*new Motor::Private(mId))
  {
  }

//-----------------------------
// initialize the motor to zero speed
void Motor::setup()
  {
  idle();
  }

//-----------------------------
// idle the motor
void Motor::idle()
  {
  mSpeed = 0;
  mDir = stopped;

  hidden.motor.setSpeed(mSpeed);
  hidden.motor.run(convertDir(mDir));
  }

//-----------------------------
// idle the motor for a while
void Motor::idleFor(int mstimeout)
  {
//  Serial.println("Idle...");
  idle();
  delay(mstimeout);
  }

//-----------------------------
// run the motor, either forward or backward,
// where the rate is 0 (off) to 255 (full)
void Motor::runAt(uint8_t dir, uint8_t rate)
  {
  mSpeed = rate;
  mDir = dir;
  int d2 = convertDir(dir);

//  Serial.print("rate=");
//  Serial.print(rate, DEC);
  hidden.motor.run(d2);
  hidden.motor.setSpeed(rate);
  }

//-----------------------------
enum
  {
  when = 10,
  steptime = 500,
  };

//-----------------------------
// run the motor, either forward or backward,
// once it's running either increase/decrease the speed
void Motor::run(uint8_t dir, uint8_t rate)
  {
  Serial.println();
  Serial.print("M");
  Serial.print(mId, DEC);
  Serial.print(": ");
  Serial.print(dir == forward ? "Forward - " : "Backward - ");
  Serial.println(rate == slower ? "slower" : "faster");
  uint8_t start;
  uint8_t end;
  uint8_t inc;
  if (rate == faster)
    {
    start = 0;
    end = 255;
    inc = 1;
    }
  else
    {
    start = 255;
    end = 0;
    inc = -1;
    }

  for (uint8_t i = start; i != end; i += inc)
    {
    runAt(dir, i);
    delay(500);
    }
  }
